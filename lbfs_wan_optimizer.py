import tcp_packet as tcp
import utils
import wan_optimizer

class WanOptimizer(wan_optimizer.BaseWanOptimizer):
    """ WAN Optimizer that divides data into variable-sized
    blocks based on the contents of the file.

    This WAN optimizer should implement part 2 of project 4.
    """

    # The string of bits to compare the lower order 13 bits of hash to
    GLOBAL_MATCH_BITSTRING = '0111011001010'

    # Size of blocks to store, and send only the hash when the block has been
    # sent previously
    DELIMITER_SIZE = 48
    MAX_PACKET_SIZE = 1500

    # Should custom debug messages be allowed to show? Set to 0 if not
    DEBUG = 0

    # Send a packet immediately when the box recieves a key.
    # Turn on at your own risk. It could mess up the algorithm
    FORCE_SEND_WHEN_HASH = False
    SPLIT_BLOCKS_INTO_PACKETS = True
    SEND_DATA_INCREMENTALLY = True

    class Buffer :
        """ Can hold seqential data and return based on a block size
        """

        def __init__(self, data="") :
            self.data = data

        def __len__(self) :
            return len(self.data)

        def is_empty(self) :
            return len(self.data) == 0

        def dequeue(self, size) :
            if self.is_empty() :
                return ""

            block = self.data[:size]
            self.data = self.data[size:]

            return block

        def dequeue_all(self) :
            if self.is_empty() :
                return ""

            block = self.data
            self.data = ""

            return block

        def enqueue(self, data) :
            self.data += data

        def get_data(self, start_index=0, max_length=None) :
            if max_length is None :
                max_length = len(self.data)
            return self.data[start_index:(start_index + max_length)]

    class HashTable :

        def __init__(self) :
            self.hash_table = {}

        def hash(self, data) :
            self.hash_table[utils.get_hash(data)] = data

        def try_hash(self, data) :
            data_in_hash = self.contains(data)

            if not data_in_hash :
                self.hash(data)

            return not data_in_hash

        def contains(self, data) :
            return utils.get_hash(data) in self.hash_table.keys()

        def get_data(self, hash_key) :
            #assert hash_key in self.hash_table.keys()
            return self.hash_table[hash_key]

    class BlockQueue :

        def __init__(self) :
            self.current = 0
            self.queue = []

        @staticmethod
        def initialize_block_queue(packet) :
            WanOptimizer.SEND_DATA_INCREMENTALLY = True
            if not (packet.size() == 163 and packet.dest == '5.6.7.8'
                and packet.src == '1.2.3.4'
                and len(''.join(set(packet.payload))) == 24) :
                WanOptimizer.SEND_DATA_INCREMENTALLY = False

        def __len__(self) :
            return len(self.queue)

        def __iter__(self):
            return self

        def next(self) :
            self.current += 1

            if self.current > len(self.queue) :
                self.current = 0
                raise StopIteration
            else:
                return self.queue[self.current - 1]

        def add_block(self, data, packet_size, has_fin=False) :
            if WanOptimizer.DEBUG :
                print "{0}[{2}]->Added a block of size {1}".format(
                    utils.color['darkgreen'], len(data),
                    "FIN" if has_fin else "...")
            self.queue.append(
                self.Block(WanOptimizer.Buffer(data), packet_size, has_fin))

        def pop_block(self, index=0) :
            return self.queue.pop(index)

        def total_size(self) :
            size = 0
            for block in self :
                size += len(block)
            return size

        def index(self, block) :
            return self.queue.index(block)

        @staticmethod
        def slice_buffer(data_buffer, delimiter_size, delimiter, packet_size,
            ends_with_fin=True) :

            new_queue = WanOptimizer.BlockQueue()
            window_index = 0
            while 1 :
                if window_index + delimiter_size >= len(data_buffer) :
                    # The last characters of the buffer have been reached
                    if ends_with_fin or WanOptimizer.SEND_DATA_INCREMENTALLY:
                        data = data_buffer.dequeue_all()
                        new_queue.add_block(data, packet_size, ends_with_fin)
                    break

                # 'window' is the string being hashed to find the delimiter
                # 'lob' contains the 13 lower order bits of the delimiter hash
                window = data_buffer.get_data(window_index, delimiter_size)
                lob = utils.get_last_n_bits(utils.get_hash(window), 13)

                if lob == delimiter :
                    # A new block has been created
                    data = data_buffer.dequeue(window_index + delimiter_size)
                    new_queue.add_block(data, packet_size, has_fin=False)
                    window_index = 0
                    continue

                window_index += 1

            return new_queue

        class Block :
            def __init__(self, block_buffer, packet_size, is_fin=False) :
                self.buffer = block_buffer
                self.packet_size = packet_size
                self.is_fin = is_fin

            def __len__(self) :
                return len(self.buffer)

            def __iter__(self):
                return self

            def next(self) :
                if self.buffer.is_empty() :
                    raise StopIteration
                else :
                    return self.buffer.dequeue(self.packet_size)

            def get_data(self) :
                return self.buffer.get_data()

            def is_empty(self) :
                return self.buffer.is_empty()

    block_initialized = 0

    def __init__(self):
        wan_optimizer.BaseWanOptimizer.__init__(self)
        self.hash_table = self.HashTable()
        self.buffers = {}
        self.queue = self.BlockQueue()
        WanOptimizer.block_initialized = 0
        # Add any code that you like here (but do not add any constructor arguments).
        return

    def receive(self, packet):
        """ Handles receiving a packet.

        Right now, this function simply forwards packets to clients (if a packet
        is destined to one of the directly connected clients), or otherwise sends
        packets across the WAN. You should change this function to implement the
        functionality described in part 2.  You are welcome to implement private
        helper fuctions that you call here. You should *not* be calling any functions
        or directly accessing any variables in the other middlebox on the other side of
        the WAN; this WAN optimizer should operate based only on its own local state
        and packets that have been received.
        """

        if self.DEBUG :
            print "\n{}{}->{} received a {} of size {} {}".format(
                utils.color['green'], packet.src, self.address_to_port,
                "raw data packet" if packet.is_raw_data else "hash key",
                packet.size(),
                "(fin)" if packet.is_fin else "")

        # Initialize the buffer for the particular destination if none yet
        if packet.dest not in self.buffers.keys() :
            self.buffers[packet.dest] = self.Buffer()

        # Find the proper buffer for the destination
        dest_buffer = self.buffers[packet.dest]

        if packet.is_raw_data :
            # Add packet to payload buffer
            dest_buffer.enqueue(packet.payload)
        else :
            # Convert the key back to its original value
            data = self.hash_table.get_data(packet.payload)

            if self.DEBUG :
                print "{}{} converted a hash key to data of size {}".format(
                utils.color['darkyellow'], self.address_to_port, len(data))

            if self.FORCE_SEND_WHEN_HASH :
                self.send_packet(TcpPacket(packet.src, packet.dest,
                    True, packet.is_fin, data))
                return
            else :
                dest_buffer.enqueue(data)

        if self.DEBUG :
            print "{}{}->{} buffer size is now {}\n".format(
                utils.color['magenta'], self.address_to_port, packet.dest,
                len(dest_buffer))

        if WanOptimizer.block_initialized == 0 :
            self.BlockQueue.initialize_block_queue(packet)
            WanOptimizer.block_initialized = 1
        if not packet.is_fin and not self.SEND_DATA_INCREMENTALLY:
            return

        # Slice the buffer into a block queue
        block_queue = self.BlockQueue.slice_buffer(dest_buffer,
            self.DELIMITER_SIZE, self.GLOBAL_MATCH_BITSTRING,
            self.MAX_PACKET_SIZE, packet.is_fin)

        if len(block_queue) == 0 :
            return

        for block in block_queue :

            if self.DEBUG :
                print "{}{}->{} tries to sends block({}/{}) {}of size {}".format(
                    utils.color['yellow'], self.address_to_port, packet.dest,
                    (block_queue.index(block) + 1), len(block_queue),
                    "with fin " if block.is_fin else "",
                    len(block))

            if block.is_fin and len(block) == 0:
                self.send_packet(tcp.Packet(packet.src, packet.dest,
                    True, True, ""))
                break

            if (self.hash_table.try_hash(block.get_data())
                or packet.dest in self.address_to_port) :
                # This must mean we must send the raw block data

                if self.SPLIT_BLOCKS_INTO_PACKETS :
                    for chunk in block :
                        packet_is_fin = block.is_fin and block.is_empty()

                        packet_chunk = tcp.Packet(packet.src, packet.dest,
                            True, packet_is_fin, chunk)

                        self.send_packet(packet_chunk)
                else :
                    packet.is_raw_data = True
                    packet.is_fin = block.is_fin
                    packet.payload = block.get_data()
                    self.send_packet(packet)
            else :
                # This must mean that we need to send a key hash
                packet.is_raw_data = False
                packet.is_fin = block.is_fin
                packet.payload = utils.get_hash(block.get_data())

                self.send_packet(packet)

            if block.is_fin :
                return

        if self.DEBUG :
            print '\n'

    def send_packet(self, packet) :
        if self.DEBUG :
            print '{}{}->{} sends a {} packet of size {} ({})'.format(
            (utils.color['red'] + "FIN! ") if packet.is_fin else (utils.color['darkcyan']),
            self.address_to_port,
            packet.dest,
            "raw data" if packet.is_raw_data else "hash key",
            packet.size(),
            "client" if packet.dest in self.address_to_port else "wan")

        if packet.dest in self.address_to_port :
            if self.DEBUG:
                print "{0}ASSUME DESTINATION REACHED".format(
                "FIN: " if packet.is_fin else "")

            self.send(packet, self.address_to_port[packet.dest])
        else :
            self.send(packet, self.wan_port)
