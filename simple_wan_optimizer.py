import tcp_packet as tcp
import utils
import wan_optimizer

class WanOptimizer(wan_optimizer.BaseWanOptimizer):
    """ WAN Optimizer that divides data into fixed-size blocks.

    This WAN optimizer should implement part 1 of project 4.
    """

    # Size of blocks to store, and send only the hash when the block has been
    # sent previously
    BLOCK_SIZE = 8000

    # Should custom debug messages be allowed to show? Set to 0 if not
    DEBUG = 0

    class Buffer :
        """ Can hold seqential data and return based on a block size
        """

        def __init__(self, block_size, data = "") :
            assert block_size > 0

            self.data = data
            self.block_size = block_size

        def __len__(self) :
            return len(self.data)

        def is_empty(self) :
            return len(self.data) == 0

        def block_is_ready(self) :
            return len(self.data) >= self.block_size

        def dequeue(self) :
            if self.is_empty() :
                return ""

            block = self.data[:self.block_size]
            self.data = self.data[self.block_size:]

            return block

        def dequeue_all(self) :
            if self.is_empty() :
                return ""

            block = self.data
            self.data = ""

            return block

        def enqueue(self, data) :
            self.data += data

        def get_data(self) :
            return self.data

    class HashTable :

        def __init__(self) :
            self.hash_table = {}

        def hash(self, data) :
            self.hash_table[utils.get_hash(data)] = data

        def try_hash(self, data) :
            data_in_hash = self.contains(data)

            if not data_in_hash :
                self.hash(data)

            return not data_in_hash

        def contains(self, data) :
            return utils.get_hash(data) in self.hash_table.keys()

        def get_data(self, hash_key) :
            #assert hash_key in self.hash_table.keys()
            return self.hash_table[hash_key]

    class BlockQueue :
        def __init__(self) :
            self.current = 0
            self.queue = []

        def __len__(self) :
            return len(self.queue)

        def __iter__(self):
            return self

        def next(self) :
            self.current += 1

            if self.current > len(self.queue) :
                self.current = 0
                raise StopIteration
            else:
                return self.queue[self.current - 1]

        def add_block(self, data, packet_size, has_fin = False) :
            self.queue.append(
                self.Block(WanOptimizer.Buffer(packet_size, data), has_fin))

        def pop_block(self, index=0) :
            return self.queue.pop(index)

        def total_size(self) :
            size = 0
            for block in self :
                size += len(block)
            return size

        @staticmethod
        def slice_buffer(data_buffer, packet_size, ends_with_fin=False) :

            new_queue = WanOptimizer.BlockQueue()
            while 1 :

                block_is_ready = data_buffer.block_is_ready()
                block_is_fin = not block_is_ready and ends_with_fin

                if not (block_is_ready or ends_with_fin) or data_buffer.is_empty() :
                    break

                new_queue.add_block(
                    data_buffer.dequeue(), packet_size, block_is_fin)

            return new_queue

        class Block :
            def __init__(self, block_buffer, is_fin=False) :
                self.buffer = block_buffer
                self.is_fin = is_fin

            def __len__(self) :
                return len(self.buffer)

            def __iter__(self):
                return self

            def next(self) :
                if self.buffer.is_empty() :
                    raise StopIteration
                else :
                    return self.buffer.dequeue()

            def get_data(self) :
                return self.buffer.get_data()

            def is_empty(self) :
                return self.buffer.is_empty()

    def __init__(self):
        wan_optimizer.BaseWanOptimizer.__init__(self)
        self.hash_table = self.HashTable()
        self.buffers = {}
        self.queue = self.BlockQueue()
        # Add any code that you like here (but do not add any constructor arguments).
        return

    def receive(self, packet):
        """ Handles receiving a packet.

        Right now, this function simply forwards packets to clients (if a packet
        is destined to one of the directly connected clients), or otherwise sends
        packets across the WAN. You should change this function to implement the
        functionality described in part 1.  You are welcome to implement private
        helper fuctions that you call here. You should *not* be calling any functions
        or directly accessing any variables in the other middlebox on the other side of
        the WAN; this WAN optimizer should operate based only on its own local state
        and packets that have been received.
        """

        if self.DEBUG :
            print "\n{}{}{} received a {} of size {} {}".format(
                utils.color['green'], utils.DEBUG_NAME, self.address_to_port,
                "raw data packet " if packet.is_raw_data else "hash key",
                packet.size(),
                "(fin)" if packet.is_fin else "")

        # Initialize the buffer for the particular destination if none yet
        if packet.dest not in self.buffers.keys() :
            self.buffers[packet.dest] = self.Buffer(self.BLOCK_SIZE)

        # Find the proper buffer for the destination
        dest_buffer = self.buffers[packet.dest]

        # Automatically send a fin packet if it and the buffer is empty
        if dest_buffer.is_empty() and (packet.is_fin and packet.size() == 0) :
            self.send_packet(packet)
            return

        if packet.is_raw_data :
            # Add packet to payload buffer
            dest_buffer.enqueue(packet.payload)
        else :
            # Convert the key back to its original value
            data = self.hash_table.get_data(packet.payload)
            dest_buffer.enqueue(data)

            if self.DEBUG :
                print "{}{} converted a hash key to data of size {}".format(
                utils.color['darkyellow'], self.address_to_port, len(data))

        if self.DEBUG :
            print "{}{}->{} buffer size is now {}\n".format(
                utils.color['magenta'], self.address_to_port, packet.dest,
                len(dest_buffer))

        ready_to_send = dest_buffer.block_is_ready() or packet.is_fin
        if not ready_to_send :
            return

        # Slice the buffer into a block queue
        block_queue = self.BlockQueue.slice_buffer(dest_buffer,
            utils.MAX_PACKET_SIZE, packet.is_fin)

        for block in block_queue :

            if self.DEBUG :
                print "{}{} tries to send a block {}of size {}".format(
                    utils.color['yellow'], self.address_to_port,
                    "with fin " if block.is_fin else "",
                     len(block))

            if (self.hash_table.try_hash(block.get_data())
                or packet.dest in self.address_to_port) :
                # This must mean we must send the raw block data

                for chunk in block :
                    packet_is_fin = block.is_fin and block.is_empty()

                    packet_chunk = tcp.Packet(packet.src, packet.dest,
                        True, packet_is_fin, chunk)

                    self.send_packet(packet_chunk)
            else :
                # This must mean that we need to send a key hash, not the data
                packet.is_raw_data = False
                packet.is_fin = block.is_fin
                packet.payload = utils.get_hash(block.get_data())

                self.send_packet(packet)

        if self.DEBUG :
            print '\n'

    def send_packet(self, packet) :
        if self.DEBUG :
            print '{}{}->{} sends a {} packet of size {} ({})'.format(
            (utils.color['red'] + "FIN! ") if packet.is_fin else (utils.color['darkcyan']),
            self.address_to_port,
            packet.dest,
            "raw data" if packet.is_raw_data else "hash key",
            packet.size(),
            "client" if packet.dest in self.address_to_port else "wan")

        if packet.dest in self.address_to_port :
            self.send(packet, self.address_to_port[packet.dest])
        else :
            self.send(packet, self.wan_port)
